import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class Gui {

	protected Shell shell;
	
	private Label availableList;
	
	

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		try {
			Gui window = new Gui();
			Game.instanceGame();
			Game.instanceGame().setGui(window);
			
			window.open();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Button btnCreateBall = new Button(shell, SWT.NONE);
		btnCreateBall.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				guiBallsCreation temp = new guiBallsCreation(shell , 0);
				temp.open();
			}
		});
		btnCreateBall.setBounds(44, 141, 75, 25);
		btnCreateBall.setText("Create Ball");
		
		Button btnLaunchBall = new Button(shell, SWT.NONE);
		btnLaunchBall.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				guiLauncher temp = new guiLauncher(shell , 0);
				temp.open(availableList.getText());
			}
		});
		btnLaunchBall.setBounds(44, 60, 75, 25);
		btnLaunchBall.setText("Launch Ball");
		
		Label lblAvailableBalls = new Label(shell, SWT.NONE);
		lblAvailableBalls.setBounds(168, 25, 110, 15);
		lblAvailableBalls.setText("Available Balls");
		
		availableList = new Label(shell, SWT.NONE);
		//availableList.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		availableList.setBounds(168, 60, 110, 153);
		
		Label lblScore = new Label(shell, SWT.NONE);
		lblScore.setBounds(327, 80, 55, 15);
		lblScore.setText("Score:");
		
		Label label = new Label(shell, SWT.NONE);
		label.setBounds(342, 114, 55, 15);
		label.setText("0");
		
		Label availableList = new Label(shell, SWT.NONE);
		availableList.setBounds(169, 60, 98, 106);
		availableList.setText("New Label");
		
		

	}
	
	public void updateAvailable(String disponibles)
	{
		System.out.println(disponibles);
		availableList.setText("");
		availableList.setText(disponibles);
	}
	@Override
    public String toString() {
        
        return "mimimimimi";

    }
}
