import javax.swing.JOptionPane;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;


public class guiBallsCreation extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text nameIn;
	private Text xIn;
	private Text yIn;
	private Text weightIn;
	

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public guiBallsCreation(Shell parent, int style) {
		super(parent, style);
		

		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents( );
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(291, 300);
		shell.setText(getText());
		Label lblName = new Label(shell, SWT.NONE);
		lblName.setBounds(10, 10, 55, 15);
		lblName.setText("Name");
		
		Label lblPosition = new Label(shell, SWT.NONE);
		lblPosition.setBounds(146, 10, 55, 15);
		lblPosition.setText("Position");
		
		xIn = new Text(shell, SWT.BORDER);
		xIn.setBounds(165, 37, 76, 21);
		
		yIn = new Text(shell, SWT.BORDER);
		yIn.setBounds(165, 94, 76, 21);
		
		Label lblWeight = new Label(shell, SWT.NONE);
		lblWeight.setBounds(10, 84, 55, 15);
		lblWeight.setText("Weight");
		
		
		
		Label lblX = new Label(shell, SWT.NONE);
		lblX.setBounds(146, 40, 13, 15);
		lblX.setText("X");
		
		Label lblY = new Label(shell, SWT.NONE);
		lblY.setBounds(146, 97, 13, 15);
		lblY.setText("Y");
		Button btnBack = new Button(shell, SWT.NONE);
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
				shell.dispose();
			}
		});
		btnBack.setBounds(10, 206, 75, 25);
		btnBack.setText("Back");
		
		Button btnCreate = new Button(shell, SWT.NONE);
		btnCreate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int x=0 , y=0 , weight=0;
				try
				{
					x=Integer.parseInt(xIn.getText());
					y=Integer.parseInt(yIn.getText());
					weight=Integer.parseInt(weightIn.getText());
				}
				catch(Exception exc)
				{
					JOptionPane.showMessageDialog(null, "Error in parsing the values");
					return ;
				}
				Game.instanceGame().ballsFabric.ballCreator(weight , nameIn.getText() , x , y );
				shell.close();
				shell.dispose();
			}
		});
		btnCreate.setBounds(166, 206, 75, 25);
		btnCreate.setText("Create");
		
		nameIn = new Text(shell, SWT.BORDER);
		nameIn.setBounds(10, 37, 76, 21);
		
		weightIn = new Text(shell, SWT.BORDER);
		weightIn.setBounds(10, 105, 76, 21);
		
		

	}
}
