import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.*;


public class SwingJuego extends JFrame implements ActionListener {

	public JButton BotonModificar;
	public JButton BotonLanzar;
	public JButton BotonVolver;
	public MyObject objeto;
	public JPanel myPanel;
	public boolean modoPrueba;
	public int mundo;
	JLabel caracteristicasObjeto;
	GridBagConstraints gbc;
	
	public SwingJuego(boolean modo, int numMundo)
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		modoPrueba = modo;
		mundo = numMundo;
		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		JLabel pastoCancha;
		
		if(modoPrueba)
		{
			objeto = new MyObject(1, 10, 10, 45, new Point(0,0));
			pastoCancha = new JLabel(new ImageIcon("Imagenes\\sky.jpg"));
		}
		
		else
		{
			ImageIcon imagenSuperficie;
			
			if(mundo == 1)
			{
				objeto = new MyObject(1, 3, 10, 45, new Point(0,0));
				imagenSuperficie = new ImageIcon("Imagenes\\SuperficieMercurio2.jpg");
			}
			
			else if(mundo == 2)
				{
					objeto = new MyObject(1, 10, 10, 45, new Point(0,0));
					imagenSuperficie = new ImageIcon("Imagenes\\SuperficieTierra.jpg");
				}
				 
				else 
				{
					objeto = new MyObject(1, 23, 10, 45, new Point(0,0));
					imagenSuperficie = new ImageIcon("Imagenes\\SuperficieJupiter.jpg");
				}
			
			Image img = imagenSuperficie.getImage();  
			Image newimg = img.getScaledInstance(400, 300,  java.awt.Image.SCALE_SMOOTH);  
			pastoCancha = new JLabel(new ImageIcon(newimg));
		}
		
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 10;
		gbc.insets = new Insets(25, 50, 50, 0);
		myPanel.add(pastoCancha, gbc);
		
		
		caracteristicasObjeto = new JLabel(objeto.informeCaracteristicas());
		gbc.gridx = 10;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 50, 0, 50);
		myPanel.add(caracteristicasObjeto, gbc);
		
		BotonLanzar = new JButton("Lanzar");
		BotonLanzar.addActionListener(this);
		gbc.gridx = 0;
		gbc.gridy = 1;
		myPanel.add(BotonLanzar, gbc);
		
		BotonModificar = new JButton("Modificar");
		BotonModificar.addActionListener(this);
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonModificar, gbc);
		
		BotonVolver = new JButton("Volver al men� principal");
		BotonVolver.addActionListener(this);
		gbc.gridx = 10;
		gbc.gridy = 1;
		gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
		myPanel.add(BotonVolver, gbc);

		this.add(myPanel);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == BotonModificar)
		{
			SwingModificar modificar = new SwingModificar(objeto, caracteristicasObjeto, myPanel);
			modificar.setTitle("MODIFICAR OBJETO");
			modificar.setSize(300, 300);
			modificar.setVisible(true);
			modificar.centreWindow();
		}
		
		else if (event.getSource() == BotonLanzar)
		{
			int tiempo = 1;
			
			while((objeto.position.x > 0 && objeto.position.y > 0) || tiempo == 1)
			{
				int auxX = (int) (objeto.inicialPosition.x + objeto.velocity*Math.cos(Math.toRadians(objeto.angle))*tiempo);
				int auxY = (int) (objeto.inicialPosition.y + objeto.velocity*Math.sin(Math.toRadians(objeto.angle))*tiempo - 0.5 * objeto.gravity*tiempo*tiempo);
				
				System.out.println(auxX);
				System.out.println(auxY);
				
				if(auxX < 0)
				{ 
					auxX = 0;
				}
				
				if(auxY < 0) 
				{ 
					auxY = 0; 
				}
				
				objeto.setPosition(new Point(auxX, auxY));
				
				caracteristicasObjeto.setText(objeto.informeCaracteristicas());
				gbc.gridx = 10;
				gbc.gridy = 0;
				gbc.insets = new Insets(0, 50, 0, 50);
				gbc.anchor = GridBagConstraints.CENTER;
				myPanel.add(caracteristicasObjeto, gbc);
				
				tiempo++;
				System.out.println(objeto.informeCaracteristicas());
			}
			
			System.out.println("listo");
			
		}
		
		else
		{
			SwingElegirModo swing = new SwingElegirModo();
			swing.setTitle("BIENVENIDOS");
			swing.setSize(500, 500);
			swing.setVisible(true);
			swing.centreWindow();
			
			this.dispose();
		}
		
	}
	
	
	
	//fuente m�todo: http://stackoverflow.com/questions/144892/how-to-center-a-window-in-java
	
	/**
	 * 
	 * este m�todo permite centrar las ventanas en la pantalla del computador
	 */
	public void centreWindow() {
	    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
	    this.setLocation(x, y);
	}
	
}
